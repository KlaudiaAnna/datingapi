import { Component, OnInit } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Response } from 'selenium-webdriver/http';

@Component({
  selector: 'app-value',
  templateUrl: './value.component.html',
  styleUrls: ['./value.component.scss']
})
export class ValueComponent implements OnInit {
  values: any;

  constructor(private htttp: HttpClient) { }

  ngOnInit() {
    this.getValues();
  }

  getValues() {
this.htttp.get('http://localhost:62307/api/values/').subscribe(response => {
this.values = response;
}, error => {
 console.log(error);
});
  }
}
